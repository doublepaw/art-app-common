export class KeyValue<T> {
    constructor(key: string = null, value: T = null) {
        this.key = key;
        this.value = value;
    }
    public key: string;
    public value: T;
}