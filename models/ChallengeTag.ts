import { ChallengeTagType } from './ChallengeTagType';

export class ChallengeTag {
  public name: string;
  public type: ChallengeTagType;
  
  constructor(name: string = "", type: ChallengeTagType = ChallengeTagType.Category) {
    this.name = name;
    this.type = type;
  }
}