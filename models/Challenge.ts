import { ChallengeTag } from './ChallengeTag';
import {ChallengeState} from './ChallengeState';

export class Challenge {
  public title: string;
  public description: string;
  public state: ChallengeState;
  public challengeTemplateId?: string;
  public challengeTagIds: string[];
  public completedDate: Date;
  public imageUrl: string;
}