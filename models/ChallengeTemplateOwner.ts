import { ChallengeTemplateOwnerType } from './ChallengeTemplateOwnerType';

export class ChallengeTemplateOwner {
  public type: ChallengeTemplateOwnerType;
  public ownerId: string;

  constructor(
    type: ChallengeTemplateOwnerType = ChallengeTemplateOwnerType.Admin,
    ownerId: string = "") {

    this.type = type;
    this.ownerId = ownerId;
  }
}