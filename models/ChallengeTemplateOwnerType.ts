export enum ChallengeTemplateOwnerType {
  Admin,
  User,
  StudyGroup
}