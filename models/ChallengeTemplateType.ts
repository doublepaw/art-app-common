export enum ChallengeTemplateType {
  Standard = 0,
  Time = 1,
  Palette = 2,
}