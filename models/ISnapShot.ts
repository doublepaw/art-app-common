export interface ISnapShot<T> {
    key: string;
    val: () =>T; 
}