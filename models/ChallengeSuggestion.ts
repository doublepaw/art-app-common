export class ChallengeSuggestion {
  public challengeTemplateId: string;
  public title: string;
  public description: string;
  public challengeTagIds: string[];
}