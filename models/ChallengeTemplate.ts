import { ChallengeTemplateType } from './ChallengeTemplateType';
import { ChallengeTemplateOwner } from './ChallengeTemplateOwner';

export class ChallengeTemplate {

  public type: ChallengeTemplateType;
  public title: string;
  public description: string;
  public challengeTagIds: string[];
  public challengeOwner: ChallengeTemplateOwner;

  constructor(
    type: ChallengeTemplateType = ChallengeTemplateType.Standard,
    title: string = "",
    description: string = "",
    owner: ChallengeTemplateOwner = null) {

    this.title = title;
    this.description = description;
    this.challengeTagIds = [];
    this.challengeOwner = owner;
    this.type = type;
  }
}

