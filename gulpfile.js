var gulp = require('gulp');
var typescript = require('gulp-typescript');

var typescriptProject = typescript.createProject('tsconfig.json');

gulp.task('default', function () {
    typescriptProject.src()
        .pipe(typescript())
        .pipe(gulp.dest('dist'));
});