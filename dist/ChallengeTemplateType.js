"use strict";
var ChallengeTemplateType;
(function (ChallengeTemplateType) {
    ChallengeTemplateType[ChallengeTemplateType["Standard"] = 0] = "Standard";
    ChallengeTemplateType[ChallengeTemplateType["Time"] = 1] = "Time";
    ChallengeTemplateType[ChallengeTemplateType["Palette"] = 2] = "Palette";
})(ChallengeTemplateType = exports.ChallengeTemplateType || (exports.ChallengeTemplateType = {}));
