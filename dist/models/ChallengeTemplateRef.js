"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var ChallengeTemplateType_1 = require("./ChallengeTemplateType");
var ChallengeTemplate_1 = require("./ChallengeTemplate");
var ChallengeTemplateRef = (function (_super) {
    __extends(ChallengeTemplateRef, _super);
    function ChallengeTemplateRef(type, title, description, id) {
        if (type === void 0) { type = ChallengeTemplateType_1.ChallengeTemplateType.Standard; }
        if (title === void 0) { title = ""; }
        if (description === void 0) { description = ""; }
        if (id === void 0) { id = ""; }
        var _this = _super.call(this, type, title, description) || this;
        _this.id = id;
        _this.challengeTagIds = [];
        return _this;
    }
    return ChallengeTemplateRef;
}(ChallengeTemplate_1.ChallengeTemplate));
exports.ChallengeTemplateRef = ChallengeTemplateRef;
