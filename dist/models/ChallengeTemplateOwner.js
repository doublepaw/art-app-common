"use strict";
var ChallengeTemplateOwnerType_1 = require("./ChallengeTemplateOwnerType");
var ChallengeTemplateOwner = (function () {
    function ChallengeTemplateOwner(type, ownerId) {
        if (type === void 0) { type = ChallengeTemplateOwnerType_1.ChallengeTemplateOwnerType.Admin; }
        if (ownerId === void 0) { ownerId = ""; }
        this.type = type;
        this.ownerId = ownerId;
    }
    return ChallengeTemplateOwner;
}());
exports.ChallengeTemplateOwner = ChallengeTemplateOwner;
