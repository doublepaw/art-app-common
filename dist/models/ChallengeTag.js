"use strict";
var ChallengeTagType_1 = require("./ChallengeTagType");
var ChallengeTag = (function () {
    function ChallengeTag(name, type) {
        if (name === void 0) { name = ""; }
        if (type === void 0) { type = ChallengeTagType_1.ChallengeTagType.Category; }
        this.name = name;
        this.type = type;
    }
    return ChallengeTag;
}());
exports.ChallengeTag = ChallengeTag;
