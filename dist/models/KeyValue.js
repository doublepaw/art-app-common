"use strict";
var KeyValue = (function () {
    function KeyValue(key, value) {
        if (key === void 0) { key = null; }
        if (value === void 0) { value = null; }
        this.key = key;
        this.value = value;
    }
    return KeyValue;
}());
exports.KeyValue = KeyValue;
