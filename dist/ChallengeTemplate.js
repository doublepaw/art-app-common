"use strict";
var ChallengeTemplateType_1 = require("./ChallengeTemplateType");
var ChallengeTemplate = (function () {
    function ChallengeTemplate(type, title, description, owner) {
        if (type === void 0) { type = ChallengeTemplateType_1.ChallengeTemplateType.Standard; }
        if (title === void 0) { title = ""; }
        if (description === void 0) { description = ""; }
        if (owner === void 0) { owner = null; }
        this.title = title;
        this.description = description;
        this.challengeTagIds = [];
        this.challengeOwner = owner;
        this.type = type;
    }
    return ChallengeTemplate;
}());
exports.ChallengeTemplate = ChallengeTemplate;
