"use strict";
var ChallengeTemplateOwnerType;
(function (ChallengeTemplateOwnerType) {
    ChallengeTemplateOwnerType[ChallengeTemplateOwnerType["Admin"] = 0] = "Admin";
    ChallengeTemplateOwnerType[ChallengeTemplateOwnerType["User"] = 1] = "User";
    ChallengeTemplateOwnerType[ChallengeTemplateOwnerType["StudyGroup"] = 2] = "StudyGroup";
})(ChallengeTemplateOwnerType = exports.ChallengeTemplateOwnerType || (exports.ChallengeTemplateOwnerType = {}));
