"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var ChallengeTagType_1 = require("./ChallengeTagType");
var ChallengeTag_1 = require("./ChallengeTag");
var ChallengeTagRef = (function (_super) {
    __extends(ChallengeTagRef, _super);
    function ChallengeTagRef(name, type, id) {
        if (name === void 0) { name = ""; }
        if (type === void 0) { type = ChallengeTagType_1.ChallengeTagType.Category; }
        if (id === void 0) { id = ""; }
        var _this = _super.call(this, name, type) || this;
        _this.id = id;
        return _this;
    }
    return ChallengeTagRef;
}(ChallengeTag_1.ChallengeTag));
exports.ChallengeTagRef = ChallengeTagRef;
